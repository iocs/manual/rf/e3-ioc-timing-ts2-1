require essioc

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet(PEVR, "TS2-010:Ctrl-EVR-101")
epicsEnvSet(PCI_SLOT,"0e:00.0")
iocshLoad("$(TOP)/timing_ts2.iocsh")

## For commands to be run after iocInit, use the function afterInit()

# Call iocInit to start the IOC
iocInit()
date
